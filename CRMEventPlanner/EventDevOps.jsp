<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="CRMEventPlanner.dao.*" %>
<%@ page import="java.util.ArrayList" %> 
<%@ page import="java.sql.*" %>  
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Create Event</title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
	</head>
	<body class="body">
	<h2>Create Event</h2>
		 <form name="formPOST" action="/CRMEventPlanner/CreateEventServlet" method="post">
			<div class="row"> 
	  			<div class="column">
			 		<font color="black"> Event name</font>
				 	<font color="red">*</font>
				 	<br/>
					 	<input style="height: 23px; width: 396px" type="text" value="Event name" name="eventName"/> 
					<br/>
					<br/>
				  	<font color="black">Category</font><font color="red">*</font> 
					<br/>
				 	<select style="height: 30px; width: 400px" name ="eventCategory"> 
				 		<option value="-1" selected>Event category </option>
						
					</select>
				
						<br/>
						<br/>
					    <font color="black">Place</font><br/>
						 	<input style="height: 23px; width: 396px" type="text" value="Place" name="eventPlace"/> 
						<br/>
						<br/>
							<font color="black">Start Date and Time</font><font color="red">*</font> 
					        <input type="datetime-local" name="eventStartDate"> 
						<br/>
 					    <br/>
							<font color="black">End Date and Time </font><font color="red">*</font> 
							<input type="datetime-local" name="eventEndDate"> 
						<br/>
			    		<br/>
			    		<font color="black">NBC - Organizer</font><br/>
						<input style="height: 23px; width: 396px" type="text" name="eventOrganizer">
						<br/>
			    		<br/>
			    		<br/>
			    		<br/>
						<input class= "menuitem" type = "submit" value = "CREATE EVENT">
					</div>
									
					<div class="column">
						 	
					  	<font color="black">Full address</font><br/>
					  	<input style="height: 23px; width: 396px" type="text" name="eventAddress">
					  	
						<br/>
					    <br/>
					 		<font color="black">Line of business</font><font color="red">*</font>
					 		<br/>
						 	<select style="height: 30px; width: 400px" name ="eventLOB"> 
					 		<option value="-1" selected>Organizing team </option>
							
					   </select>
					   <br/>
					   <br/>
					   <font color="black">Key company</font><font color="red">*</font> 
					   <br/>
					   <select style="height: 30px; width: 400px" name ="eventCompany"> 
					 		<option value="-1" selected>Key company</option> 
							
						</select>
						<br/>
						<br/>
							<font color="black">City</font><font color="red">*</font> 
						<br/>
					 	<select style="height: 30px; width: 400px" name ="eventCity"> 
					 		<option value="-1" selected>City</option>
					 		
					</select>
					<br/>
					<br/>
					<font color="black">Maximum Capacity</font><br/>
					<input style="height: 23px; width: 396px" type="text" name="eventCapacity">
					<br/>
				    <br/>
					<font color="black">Reservation</font><br/>
					<input style="height: 23px; width: 396px" type="text" name="eventReservation">
				</div>
				<br/>
			    <br/>
			</div>
			<br/>
			<br/>	
	
	</form>
	</body>
</html>