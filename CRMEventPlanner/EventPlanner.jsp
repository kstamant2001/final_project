<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>NBC Events</title>
</head>
<body>

   <div>
        <div class="tabs-container">
            <ul class="nav nav-tabs sub-tabs">
                <li class="active"><a data-toggle="tab" href=CreateEvent.jsp><i class="fa fa-calendar"></i> Event</a></li>
                <li><a data-toggle="tab" href="#tab-attendees"><i class="fa fa-envelope-open-o"></i> Attendees</a></li>
                <li><a data-toggle="tab" href="#tab-timeslots"><i class="fa fa-clock-o"></i> Schedule</a></li>
            </ul>
            </div>
            <div class="tab-content" style="padding-bottom: 20px;margin-top: 20px">
                <div id="tab-event" class="tab-pane active">
            </div>

        </div>
    </div>
   </body>
</html>