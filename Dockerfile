FROM tomcat:9.0.20-jdk8-slim

RUN mkdir /usr/local/tomcat/webapps/projet/
COPY CRMEventPlanner/EventDevOps.jsp  /usr/local/tomcat/webapps/projet/index.jsp
COPY CRMEventPlanner/style.css  /usr/local/tomcat/webapps/projet/style.css

CMD ["catalina.sh", "run"]