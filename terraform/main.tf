terraform {
  backend "s3" {
    bucket = "bnc-karine-terraform-states"
    key    = "terraform-training"
    region = "us-east-1"
  }
}
provider "aws" {
 region = "us-east-1"
}
resource "aws_key_pair" "authorized_key" {
  key_name   = "terraform-ks"
  public_key = "${var.authorized_key}"
}
resource "aws_instance" "lab1karine" {
 count         = "${var.instances_count}"
 ami           = "ami-2757f631"
 key_name      = "${aws_key_pair.authorized_key.key_name}"
 instance_type = "t2.micro"
}
resource "ansible_host" "default" {
  count = "${aws_instance.lab1karine.count}"
  inventory_hostname = "${aws_instance.lab1karine.*.id[count.index]}"
  vars {
    ansible_user = "ubuntu"
    ansible_host = "${aws_instance.lab1karine.*.public_ip[count.index]}"
  }
}
