variable "authorized_key" {
  type = "string"
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDm8IFUtqMjwj2BIBcxpxmMvmJRpeewG84HYKix+oXXOHy3t+xSQfqjMDgDUPS8E95bvHMy9tC1A9tiBcqb2mYPJhnmmaNh+Wq5ja2Y45UipRHt/kAtLIc+Tu455YTnYd2eDbcL37BEpjEZqAfWan+YrcEZ6UyeFnJURgHDU2qivGVyJpn+Q7HuHRtSwAw/WdKrz0/47xK0cTcl6NdeVTJqMYmdovotoyot+hammbu+fcNdCskUQwPHQoFnpm2USJ/7v1nMZfA+H2NFsq9Svdzh8RIEb4AVaOfB9x04dOp8rL5kg1rqFLPu0qkYdibSMw4munGHmCzPLRzicNRNtMh3"
}

variable "instances_type" {
  default = "t2.micro"
}

variable "instances_count" {
  default = 1
}

variable "tags" {
  type = "map"
  default = {
    demo = true
  }
}
