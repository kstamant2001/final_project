package CRMEventPlanner.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class CRMConnection {
	private Connection con = null;
//    private ResultSet rs = null;
    private static CRMConnection instance = null;
    protected static com.mysql.jdbc.Driver sDriverInstance;
    private static final String DB_DRIVER = "db.connection.driver";
	private static final String DB_URL = "db.connection.url";
	private static final String DB_USER = "db.connection.user";
	private static final String DB_PASSWORD = "db.connection.password";
	private String driver = "";
	private String url = "";
	private String user = "";
	private String pwd = "";
    
    public CRMConnection () {
    	try
    	{
    		InputStream input = CRMConnection.class.getClassLoader().getResourceAsStream("CRMEventPlanner-pp.properties");
    		Properties prop = new Properties();
    		prop.load(input);
    		driver = prop.getProperty(DB_DRIVER);
    		url = prop.getProperty(DB_URL);
    		user = prop.getProperty(DB_USER);
    		pwd = prop.getProperty(DB_PASSWORD);
    	}
    	catch (IOException ex)
		{
			
		}	
    }
    
    public static CRMConnection getInstance() {
        if(instance == null) {
            instance = new CRMConnection();
        }
        return instance;
    }
    
    public void DbConnection (String URL, String user, char[] password){
        
        getDriver();
    }
	
	public Connection getConnection () throws SQLException {
		try
		{	
			getConnection(url, user, pwd);
			
		}	
		catch (SQLException sqe)
		{
		
		}
		return con;
	}
		
	public void getConnection(String dbUrl, String user, String pass) throws SQLException {    
        try
        {  
        	Class.forName(driver);
        }
        catch (ClassNotFoundException e) {
        }
        
        try
        {
            con = DriverManager.getConnection(dbUrl, user, pass);
        }
        catch (SQLException sqe){
            throw new SQLException(sqe);
        }
	}
		
    protected void closeResultSet(ResultSet rs) throws SQLException {
        if(rs != null) {
            try {
                    rs.close();

            } 
            catch (SQLException sqe){
                
            }
        }
    }
	    
    protected void closeStatement(PreparedStatement stmt) throws SQLException {
        if(stmt != null) {
            try {
                    stmt.close();

            }
            catch (SQLException sqe){
            }
        }
    }
    protected void closeStatement(Statement stmt) throws SQLException {
        if(stmt != null) {
            try {
                    stmt.close();

            }
            catch (SQLException sqe){
            }
        }
    }
	    
    protected void closeCallStmt(CallableStatement cs) throws SQLException {
        if(cs != null) {
            try {
                cs.close();
            }
            catch (SQLException sqe){
            }
        }
    }
	    
    protected void closeConnection(Connection connection) throws SQLException {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException sqe){
            }
        }
    }
	    
    public void getDriver () {
        if (sDriverInstance != null) return;
        try {
          sDriverInstance = new com.mysql.jdbc.Driver();
         DriverManager.registerDriver(sDriverInstance);
       }
       catch (SQLException sqe){
       }
   } 
}
