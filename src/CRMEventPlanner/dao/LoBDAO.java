package CRMEventPlanner.dao;
import java.sql.*;
import java.util.ArrayList;

public class LoBDAO {
	private static LoBDAO instance = null;
    private Connection con = null;
    private PreparedStatement stmt;
    private ResultSet rs = null;
    protected static com.mysql.jdbc.Driver sDriverInstance;
	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://LRTCCCAMFY0493.cloud.res.bngf.local:3306";
	private static final String DB_USER = "barm066";
	private static final String DB_PASSWORD = "barm066";
		
	public static LoBDAO getInstance() {
        if(instance == null) {
            instance = new LoBDAO();
        }
        return instance;
    }
		
	public void DbConnection (String URL, String user, char[] password){
	        
        getDriver();
    }
	
	public void getConnection () throws SQLException {
		try
		{
			getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			
		}	
		catch (SQLException sqe)
		{
			
		}
	}
		
	public void getConnection(String dbUrl, String user, String pass) throws SQLException {    
        try
        {  
        	Class.forName(DB_DRIVER);
        }
        catch (ClassNotFoundException e) {
        }
        
        try
        {
            con = DriverManager.getConnection(dbUrl, user, pass);
        }
        catch (SQLException sqe){
            throw new SQLException(sqe);
        }
	}
		
    protected void closeResultSet(ResultSet rs) throws SQLException {
        if(rs != null) {
            try {
                    rs.close();

            } 
            catch (SQLException sqe){
                
            }
        }
    }
	    
    protected void closeStatement(PreparedStatement stmt) throws SQLException {
        if(stmt != null) {
            try {
                    stmt.close();

            }
            catch (SQLException sqe){
            }
        }
    }
	    
    protected void closeCallStmt(CallableStatement cs) throws SQLException {
        if(cs != null) {
            try {
                cs.close();
            }
            catch (SQLException sqe){
            }
        }
    }
	    
    protected void closeConnection(Connection connection) throws SQLException {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException sqe){
            }
        }
    }
	    
    public void getDriver () {
        if (sDriverInstance != null) return;
        try {
          sDriverInstance = new com.mysql.jdbc.Driver();
         DriverManager.registerDriver(sDriverInstance);
       }
       catch (SQLException sqe){
       }
   } 
   public ArrayList<String> SelectLOB() throws SQLException {
	   String sql = "SELECT distinct Description FROM salesforce.luRevenueLOB order by Description asc";
	   try {     
			getConnection();
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			ArrayList<String> LOBList = new ArrayList<String>();        
			while (rs.next()){
				LOBList.add(rs.getString("Description"));
			}
			return LOBList;
		}
		catch (SQLException sqe){
			throw new SQLException(sqe);
		}
		finally {
		closeResultSet(rs);
		closeStatement(stmt);
		closeConnection(con);
		}
   }
}
