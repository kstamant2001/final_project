package CRMEventPlanner.dao;
import java.sql.*;
import java.util.ArrayList;

import CRMEventPlanner.bean.EventBean;

public class EventDAO {

	private Connection con = null;
	private PreparedStatement pStmt;
    private Statement stmt;
	private ResultSet rs = null; 
	
   public ArrayList<String> SelectEvents () throws SQLException {
	   CRMConnection CRMcon = CRMConnection.getInstance();
	   String sql = "SELECT * FROM salesforce.events ";
	   try 
	   {  
		   con = CRMcon.getConnection();
		   pStmt = con.prepareStatement(sql);
		   rs = pStmt.executeQuery();
		   
		   ArrayList<String> EventList = new ArrayList<String>();        
		   while (rs.next()){
				EventList.add(rs.getString("name"));
		   }
			return EventList;
		}
		catch (SQLException sqe){
			throw new SQLException(sqe);
		}
		finally {
			CRMcon.closeResultSet(rs);
			CRMcon.closeStatement(pStmt);
			CRMcon.closeConnection(con);
		}
   }
   public void InsertEventB(EventBean eventBean) throws SQLException {
	   CRMConnection CRMcon = CRMConnection.getInstance();
	   String sql = "INSERT INTO salesforce.events(name, account_id, category_id, lob, organizer, max_places, place, city_id, address, start_datetime, end_datetime ,reservation, comments ) " +
		   		//"VALUES('" + eventBean.getEventName() +"','" + eventBean.getEventCompany() + "','" + eventBean.getEventCategoryId() + "','" + eventBean.getEventLOB()+ "','" + eventBean.getEventOrganizer()+ "','" + eventBean.getEventCapacity()+ "','" + eventBean.getEventPlace()+ "','" + eventBean.getEventCity()  + "','" + eventBean.getEventAddress() + "','" + eventBean.getEventStartDate()  + "','" + eventBean.getEventEntDate()+ "','" + eventBean.getEventReservation() + "','" + eventBean.getEventComments() + "')";
		   		"VALUES('" + eventBean.getEventName() +"','" + eventBean.getEventCompany() + "','" + eventBean.getEventCategory() + "','" + eventBean.getEventLOB()+ "','" + eventBean.getEventOrganizer()+ "'," + eventBean.getEventCapacity()+ ",'" + eventBean.getEventPlace()+ "','" + eventBean.getEventCity()  + "','" + eventBean.getEventAddress() + "','" + eventBean.getEventStartDate()  + "','" + eventBean.getEventEndDate()+ "'," + eventBean.getEventReservation() + ",'" + eventBean.getEventComments() + "')";
	   try {     
		   con = CRMcon.getConnection();
		   stmt = con.createStatement();
		   stmt.executeUpdate(sql);
		}
		catch (SQLException sqe){
			throw new SQLException(sqe);
		}
		finally {
			CRMcon.closeResultSet(rs);
			CRMcon.closeStatement(stmt);
			CRMcon.closeConnection(con);
		}
   }
}
