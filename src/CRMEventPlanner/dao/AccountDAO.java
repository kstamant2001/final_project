package CRMEventPlanner.dao;
import java.sql.*;
import java.util.ArrayList;
//import CRMEventPlanner.bean.EventBean;

public class AccountDAO {
    private Connection con = null;
    private PreparedStatement stmt;
    private ResultSet rs = null;
    
   public ArrayList<AccountMap> SelectCompany() throws SQLException {
	   CRMConnection CRMcon = CRMConnection.getInstance();
	   String sql = "SELECT account_name, account_id FROM salesforce.accounts order by account_name asc";
	   try {     
		    con = CRMcon.getConnection();
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			ArrayList<AccountMap> CompanyList = new ArrayList<AccountMap>();        
			while (rs.next()){
				CompanyList.add(new AccountMap(rs.getString("account_name"), rs.getString("account_id")));
			}
			return CompanyList;
		}
		catch (SQLException sqe){
			throw new SQLException(sqe);
		}
		finally {
			CRMcon.closeResultSet(rs);
			CRMcon.closeStatement(stmt);
			CRMcon.closeConnection(con);
		}
   }
}
