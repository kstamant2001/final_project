package CRMEventPlanner.dao;
import java.sql.*;
import java.util.ArrayList;
import java.lang.Integer;

//import CRMEventPlanner.bean.EventBean;

public class CategoryDAO {
	private Connection con = null;
	private PreparedStatement stmt;
	private ResultSet rs = null;

	
	
		
   public ArrayList<CategoryMap> SelectCategories() throws SQLException {
	   CRMConnection CRMcon = CRMConnection.getInstance();
	   String sql = "SELECT category, id FROM salesforce.luEventCategories";
	   try {     
			con = CRMcon.getConnection();
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();

			ArrayList<CategoryMap> CategoriesList = new ArrayList<CategoryMap>();        
			while (rs.next()){
				
				CategoriesList.add(new CategoryMap(rs.getString("category"), Integer.parseInt(rs.getString("id"))));
			}
			return CategoriesList;
		}
		catch (SQLException sqe){
			throw new SQLException(sqe);
		}
		finally {
			CRMcon.closeResultSet(rs);
			CRMcon.closeStatement(stmt);
			CRMcon.closeConnection(con);
		}
   }
}
