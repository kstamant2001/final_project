package CRMEventPlanner.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
//import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import CRMEventPlanner.bean.EventBean;
//import CRMEventPlanner.dao.CategoryDAO;
import CRMEventPlanner.dao.EventDAO;

/**
 * Servlet implementation class CreateEventServlet
 */
@WebServlet("/CreateEventServlet")
public class CreateEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateEventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String eventName = request.getParameter("eventName");
		
		try {
            EventDAO dbc = new EventDAO();
            EventBean event = new EventBean();
            //dbc.getConnection();
            event.setEventName(request.getParameter("eventName"));
            event.setEventCategory(request.getParameter("eventCategory"));          
            event.setEventPlace(request.getParameter("eventPlace"));
            event.setEventAddress(request.getParameter("eventAddress"));
            event.setEventCompany(request.getParameter("eventCompany"));
            event.setEventCity(request.getParameter("eventCity"));
            event.setEventLOB(request.getParameter("eventLOB"));
            event.setEventOrganizer(request.getParameter("eventOrganizer"));
            event.setEventCapacity(Integer.parseInt(request.getParameter("eventCapacity")));
            event.setEventStartDate(request.getParameter("eventStartDate"));
            event.setEventEndDate(request.getParameter("eventEndDate"));
            event.setEventReservation(Integer.parseInt(request.getParameter("eventReservation")));
            event.setEventComments(request.getParameter("eventComments"));
            dbc.InsertEventB(event);
        }
        catch (SQLException ex) {
        }
		// doGet(request, response);
		PrintWriter out = response.getWriter();
		out.println("<html><body> Event " + eventName+ " created" + "</body></html>");
	}
}
